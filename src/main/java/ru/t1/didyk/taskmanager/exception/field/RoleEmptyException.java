package ru.t1.didyk.taskmanager.exception.field;

public final class RoleEmptyException extends AbstractFieldException {

    public RoleEmptyException() {
        super("Error! Role is empty.");
    }

}
