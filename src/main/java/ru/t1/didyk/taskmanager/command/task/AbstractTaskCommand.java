package ru.t1.didyk.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.service.IProjectTaskService;
import ru.t1.didyk.taskmanager.api.service.ITaskService;
import ru.t1.didyk.taskmanager.command.AbstractCommand;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @NotNull
    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void renderTasks(@NotNull final List<Task> tasks) {
        int index = 1;
        for (@Nullable Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
