package ru.t1.didyk.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.service.IProjectService;
import ru.t1.didyk.taskmanager.api.service.IProjectTaskService;
import ru.t1.didyk.taskmanager.command.AbstractCommand;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    protected void showProject(@Nullable final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
